<!--
## Showcase Presentation Issue To-Do List
(_NOTE: This section can be removed when the issue is ready for creation_)
- [ ] Issue Title should be labeled with `Ops Showcase Presentation: ` and then a brief title of the issue worked on.
- [ ] Link issue to current month's showcase issue
- [ ] Assign yourself to the issue.
- [ ] Add applicable devops section, group and FY labeling to issue.
- [ ] Share on #ci-section at https://gitlab.slack.com/archives/C05B0MER7LM for visibility and collaboration on discussion.
- [ ] Add due date to be the last day of current month.
-->
## :sparkles: Summary of the issue 
<!--
_NOTE: This area should contain a brief summary of what the issue attempts to solve._
-->

## :key: Relevant Details

- Implementation issue:
- Video link: 
- Slides:
- Demo project:

## :question: Questions 
_NOTE: Using the format noted below, each question should be threaded. Answers can be placed as subpoints under each threaded question, tagging the original team member who asked the question_

`{Insert Question} - {tag presenter username}`

/label ~"section::ops" ~"team activities"
/relate {ADD CURRENT MONTH'S ISSUE LINK HERE}
